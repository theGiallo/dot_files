# dot_files
My dot files with scripts to gather and install them.

Now working with:

 - vim (with plugins)
 - tmux (with plugins)
 - bash (.profile and .bashrc)

## Content
 - gather_dot_files.sh
 - install_dot_files.sh

## Usage

- gather_dot_files.sh

        gather_dot_files.sh [-h|--help] [-v|--vim] [-t|--tmux] [-b|--bash]
        
            -h|--help    print this help
        
            -v|--vim     gather vim stuff
            -t|--tmux    gather tmux stuff
            -b|--bash    gather bash stuff
        
        no options gathers all stuff


- install_dot_files.sh

        install_dot_files.sh [-h|--help] [-v|--vim] [-t|--tmux] [-b|--bash]
                             [--target $TARGET]
        
            -h|--help            print this help
        
            -v|--vim             gather vim stuff
            -t|--tmux            gather tmux stuff
            -b|--bash            gather bash stuff
        
        --target $TARGET     sets the install target directory to
                             $TARGET. Usually a home, default is $HOME
        
        No options gathers all stuff.
        This script backs up old stuff appending a date and '.bkp', e.g.
        '~/.vim' is backed up as '~/.vim.2017-04-23_15-03-38.bkp'

