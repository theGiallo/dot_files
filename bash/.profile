# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# add to PATH android tools
export PATH="/home/thegiallo/Programming/Android/android-sdk-linux/platform-tools/:/home/thegiallo/Programming/Android/android-ndk-r10c/:/home/thegiallo/Programming/Android/android-sdk-linux/tools/:$PATH"
export KDEDIRS="/media/Dati/Programmi/krita/inst:$KDEDIRS"
export PATH="/media/Dati/Programmi/krita/inst/bin:$PATH"
export PATH="/media/Dati/Programmi/obs:$PATH"
export PATH="/usr/sbin/:$PATH"
export PATH="/sbin/:$PATH"
export PATH="/home/thegiallo/scripts/:$PATH"
export PATH="/home/thegiallo/Programmi/st/bin:$PATH"
export PATH="/home/thegiallo/Programmi/vim/bin:$PATH"
export PATH="/home/thegiallo/Programmi/cloc/cloc-1.70:$PATH"

# language
export LC_MESSAGES=it_IT.UTF-8

# libraries location
export LD_LIBRARY_PATH='.;'
