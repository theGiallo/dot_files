set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" This is the Vundle package, which can be found on GitHub.
" For GitHub repos, you specify plugins using the
" 'user/repository' format
Plugin 'gmarik/vundle'

" filesys explorer
Plugin 'scrooloose/nerdtree.git'

" comments!
Plugin 'scrooloose/nerdcommenter'

" syntax error checker
Plugin 'scrooloose/syntastic.git'

" To get plugins from Vim Scripts, you can reference the plugin
" by its name as it appears on the site
Plugin 'Buffergator'

" git commands
Plugin 'tpope/vim-fugitive'

" comment/uncomment with 'gcc' or 'gc'
Plugin 'tpope/vim-commentary'

" surround things with things
Plugin 'tpope/vim-surround'

" Color themes base-16
Plugin 'chriskempson/base16-vim'

" reruns ctags at write file
"Plugin 'vim-scripts/AutoTag'
Plugin 'craigemery/vim-autotag'

" better status line
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" fuzzy search of files
Plugin 'kien/ctrlp.vim'

" interact with tmux from inside vim
Plugin 'benmills/vimux'

" make tmux pass Focus events to vim
Plugin 'tmux-plugins/vim-tmux-focus-events'

" highlight and surroundings for .tmux.conf
Plugin 'tmux-plugins/vim-tmux'

" auto triggers completion box
" Plugin 'othree/vim-autocomplpop'

" informed ocmpletion from clang, jedi for other things
Plugin 'Valloric/YouCompleteMe'

" restore Focus events in tmux and iTerm (that I don't have)
Plugin 'sjl/vitality.vim'

" snippet engine in python
Plugin 'SirVer/ultisnips'

" a lot of snippets!
Plugin 'honza/vim-snippets'

" to fix integration ultisnip+ycm
Plugin 'ervandew/supertab'

" useful mappings
Plugin 'tpope/vim-unimpaired'

" Improved syntax highlight for C++
Plugin 'octol/vim-cpp-enhanced-highlight'
" Operators highlight
Plugin 'Valloric/vim-operator-highlight'

" automatically saves your vim session :Obsess to start the saving :Obsess! to
" stop
Plugin 'tpope/vim-obsession'

Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'shime/vim-livedown'
" Plugin 'JamshedVesuna/vim-markdown-preview'

" yank history!
"Plugin 'vim-scripts/YankRing.vim'

"Plugin 'nathanaelkane/vim-indent-guides'

" tabs to indent spaces to align
" Plugin 'vim-scripts/Smart-Tabs'

" context aware highlighting and renaming ( requires a compilation database,
" I'm too lazy, let's wait for YCM to implement renaming)
"Plugin 'bbchung/clighter'

" GLSL
Plugin 'tikhomirov/vim-glsl'

call vundle#end()
" Now we can turn our filetype functionality back on
filetype plugin indent on
