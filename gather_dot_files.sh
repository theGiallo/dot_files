#!/bin/bash

ALL=false
VIM=false
TMUX=false
BASH=false
ERROR=false
HELP=false

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-h|--help)
			HELP=true
		;;
		-v|--vim)
			VIM=true
		;;
		-t|--tmux)
			TMUX=true
		;;
		-b|--bash)
			BASH=true
		;;
		*)
			echo "Unknown option "$1
			ERROR=true
		;;
	esac
	shift
	done;
fi

if $ERROR || $HELP
then
	echo "Usage:"
	echo "gather_dot_files.sh [-h|--help] [-v|--vim] [-t|--tmux] [-b|--bash]"
	echo
	echo -e "    -h|--help    print this help"
	echo
	echo -e "    -v|--vim     gather vim stuff"
	echo -e "    -t|--tmux    gather tmux stuff"
	echo -e "    -b|--bash    gather bash stuff"
	echo
	echo "no options gathers all stuff"

	if $ERROR
	then
		exit 1
	fi
	if $HELP
	then
		exit 0
	fi
fi


if $ALL || $VIM
then
	cp ~/.vimrc vim/
	cp -t vim/.vim/ \
	    ~/.vim/plugins.vim
	cp -t vim/.vim/syntax/ \
	    ~/.vim/syntax/cpp.vim \
	    ~/.vim/syntax/c.vim
	cp -t vim/.vim/spell/ \
	    ~/.vim/spell/it.utf-8.spl \
	    ~/.vim/spell/en.utf-8.add \
	    ~/.vim/spell/en.utf-8.add.spl
fi

if $ALL || $TMUX
then
	cp -t tmux/ ~/.tmux.conf

	echo -n > tmux/plugins_remotes.txt
	old_wd="$PWD"
	OIFS="$IFS"
	IFS=$'\n'
	for f in `ls -1 ~/.tmux/plugins/`
	do
		if [ -d ~/.tmux/plugins/"$f" ]
		then
			cd ~/.tmux/plugins/$f
			git remote get-url origin >> $old_wd/tmux/plugins_remotes.txt
			cd $old_wd
		fi
	done
	IFS="$OIFS"
fi

if $ALL || $BASH
then
	cp -t bash/ \
	    ~/.bashrc \
	    ~/.profile
fi
