set nocompatible

" Syntastic config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" for templates
let g:syntastic_cpp_check_header = 1
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = '-std=c++11'
let g:syntastic_c_checkers = ['clang_check']
let g:syntastic_cpp_checkers = ['clang_check']
let g:syntastic_cpp_checkers = ['clang_check']
let g:syntastic_clang_check_config_file = '.syntastic_clang_check_config'
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" airline config
let g:airline_powerline_fonts = 1

"let g:airline#extensions#whitespace#enabled = 0
" spaces are allowed after tabs, but not in between
" this algorithm works well with programming styles that use tabs for
" indentation and spaces for alignment
let g:airline#extensions#whitespace#mixed_indent_algo = 2
let g:airline#extensions#whitespace#checks = [ 'trailing', 'long' ]
"let g:airline#extensions#whitespace#checks = [ 'indent', 'trailing', 'long' ]

" base16-vim plugin config
let base16colorspace=256  " Access colors present in 256 colorspace

" leader to space
let mapleader="\<Space>"

" Load the plugins
let g:vundle#lazy_load=0
source ~/.vim/plugins.vim

" stupid vim-latex
source ~/.vim/ftplugin/tex.vim

" Theme
set background=dark
colorscheme base16-monokai " I've made the SpecialKey and NotText use the color 01 instead of 03 (darker)

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-f>"
let g:UltiSnipsJumpForwardTrigger="<c-d>"
let g:UltiSnipsJumpBackwardTrigger="<c-g>"

" YouCompleteMe
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_disable_for_files_larger_than_kb = 4096
let g:ycm_register_as_syntastic_checker = 1 "default 1
let g:ycm_show_diagnostics_ui = 1 "to disable syntastic checkers for c-family
nnoremap <leader>f :YcmCompleter GoTo<CR>
let g:ycm_error_symbol = '✗'
let g:ycm_warning_symbol = '!➤'
" relative paths are relative to the cwd and not to the file with this =1
let g:ycm_filepath_completion_use_working_dir = 1
" let g:ycm_key_list_select_completion=['<Enter>']
" let g:ycm_key_invoke_completion = '<C-Space>'

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
" let g:UltiSnipsExpandTrigger = "<c-space>"
" let g:UltiSnipsJumpForwardTrigger = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" If you want :UltiSnipsEdit to split your window.
" let g:UltiSnipsEditSplit="vertical"

" snippets configuration
" ActivateAddons vim-snippets ultisnips

" CtrlP configuration
" this is a hack
"map <C-p> <C-p>
let g:ctrlp_map='<leader>pf'
" unm <C-p>
nnoremap <leader>pa     :CtrlPMixed<CR>
"nnoremap <leader>p<C-a> :CtrlPMixed<CR>
nnoremap <leader>pb     :CtrlPBuffer<CR>
"nnoremap <leader>p<C-b> :CtrlPBuffer<CR>
nnoremap <leader>pf     :CtrlP<CR>
"nnoremap <leader>p<C-f> :CtrlP<CR>
nnoremap <leader>pd     :CtrlPDir<CR>
"nnoremap <leader>p<C-d> :CtrlPDir<CR>
nnoremap <leader>pt     :CtrlPTag<CR>
"nnoremap <leader>p<C-t> :CtrlPTag<CR>
" ignore those paths
if exists("g:ctrlp_user_command")
	unlet g:ctrlp_user_command
endif
if exists("g:ctrlp_custom_ignore")
	unlet g:ctrlp_custom_ignore
endif
let g:ctrlp_custom_ignore = {
\'dir': '\v[\/]\.(git|hg|svn)$',
\'file': '\v\.(exe|so|dll)|(.*\~)$',
\}
let g:ctrlp_show_hidden = 0

" NERDTree config
" do not close NERDTree after a file is opened
let g:NERDTreeQuitOnOpen=0
" show hidden files in NERDTree
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.vim$', '\~$', 'swo$', 'swp$', 'swn$', '\.meta']
nmap <leader>j :NERDTreeToggle<CR>
nmap <leader>h :NERDTreeFind<CR>

" vim-markdown-preview
" let vim_markdown_preview_github=1
" let vim_markdown_preview_hotkey='<leader>m'
" " To display images automatically on buffer write.
" let vim_markdown_preview_toggle=1
" " let vim_markdown_preview_browser='x-www-browser'
" let vim_markdown_preview_use_xdg_open=1

" vim-livedown
nmap <leader>m :LivedownPreview<CR>
let g:livedown_open = 1
let g:livedown_autorun = 0
let g:livedown_port = 1337


" YankRing
let g:yankring_replace_n_pkey = '<C-p>'
let g:yankring_replace_n_nkey = '<C-P>'


" Exit immediately from modes (if using tmux put this in its config `set -s escape-time 0`)
set ttimeoutlen=0
set timeoutlen=1000

" silent and then redraw
command! -nargs=1 Silent
\ | execute ':silent '.<q-args>
\ | execute ':redraw!'

" don't have to press shift
nnoremap ; :

" moving up and down work as you would expect
nnoremap <silent> j gj
nnoremap <silent> k gk

" navigate easier
map <C-h> ^
map <C-l> $
map <C-j> <C-d>
map <C-k> <C-u>
" for ic modes
map! <C-h> <Home>
map! <C-l> <End>
map! <C-j> <Pagedown>
map! <C-k> <Pageup>

" so we can map <A-...>
execute "set <A-h>=\eh"
execute "set <A-l>=\el"
execute "set <A-j>=\ej"
execute "set <A-k>=\ek"
execute "set <A-t>=\et"
execute "set <A-b>=\eb"

" navigate through windows easier
map <A-h> <C-w>h
map <A-l> <C-w>l
map <A-j> <C-w>j
map <A-k> <C-w>k
map <A-t> <C-w>t
map <A-b> <C-w>b
" for ic modes
map! <A-h> <C-w>h
map! <A-l> <C-w>l
map! <A-j> <C-w>j
map! <A-k> <C-w>k
map! <A-t> <C-w>t
map! <A-b> <C-w>b

" acquires root privileges on write
cmap w!! w !sudo tee % >/dev/null

" fast save
nnoremap <leader>w :w<cr>

set spell spelllang=en_gb

" lower case = anycase
set ignorecase
set smartcase

" incremental search on (highlight results while typing)
set is

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hls
endif

" clears search results highlighting
nmap <silent> ,/ :nohls<CR>

set autoread

function! g:ToggleMouse()
	if !exists("s:old_mouse")
		let s:old_mouse = "a"
	endif

	if &mouse == ""
		let &mouse = s:old_mouse
		set list
		echo "Mouse is for Vim (" . &mouse . ")"

	else
		let s:old_mouse = &mouse
		let &mouse=""
		set nolist
		echo "Mouse is for terminal"
	endif
endfunction

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
	set mouse=a
	map <F3> :call g:ToggleMouse()<CR>
endif

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
  set nobackup " do not keep a backup file, use versions instead
else
  set backup " keep a backup file (restore to previous version)
  set undofile " keep an undo file (undo changes after closing)
endif
set history=1000 " keep 50 lines of command line history
set ruler " show the cursor position all the time
set showcmd " display incomplete commands
set shiftround " uses multiple of shiftwidth when indenting with </>
set shiftwidth=4
set tabstop=4
set softtabstop=0
set autoindent
set smartindent
set copyindent
set noexpandtab
set preserveindent
" cino
" tN for return type of function: cino=t0
" (N in unclosed parentheses (0
" uN same as (N but for 1 level deeper u0
" UN do not ignore the indenting specified by ( or u in case that the unclosed parentheses is the first non-white character in its line. U1
" wN align with the first character after unclosed parentheses w1
" WN when unclesed parentheses is the last char of line indent of N W3
set cinoptions=t0,(0,u0,U1,w2,W3

" when pasting multiple lines switch to pastemode
set pastetoggle=<F2>

" show whitespaces
set list

"set listchars=tab:├─,trail:.,extends:#,nbsp:.
"set listchars=tab:⟜─,trail:.,extends:#,nbsp:.
set listchars=tab:⊳─,trail:∙,extends:#,nbsp:∙,space:∙,eol:↵

" show matching parenthesis
set showmatch

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

" Line numbers on the left
set nu

" Gererally no relative line numbers
set nornu

" toggle relative line numbers with <C-n>
function! NumberToggle()
  if(&relativenumber == 1)
    set nornu
  else
    set rnu
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>

set encoding=utf-8

" keep 1 line below and above the cursor
set scrolloff=1

" always show the statusline
set laststatus=2

" share clipboard with system
set clipboard=unnamed

" hides buffers instead of closing them (so you do not have to save)
set hidden

" close file without deleting split
nnoremap <leader>k :bp\|bd #<CR>

if !exists("custom_autocommands_loaded")
	let custom_autocommands_loaded = 1

	" swaps caps lock and ESC on startup, and reset at closedown (requires
	" xorg-xmodmap package installed)
	fu! CapsToEsc()
		:!xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape' -e 'keycode 0x9 = Caps_Lock' 2>/dev/null 1>/dev/null
	endfunction
	fu! CapsRestore()
		:!xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock' -e 'keycode 0x9 = Escape' 2>/dev/null 1>/dev/null
	endfunction
	au VimEnter *    Silent call CapsToEsc()
	au FocusGained * Silent call CapsToEsc()
	au VimLeave *    Silent call CapsRestore()
	au FocusLost *   Silent call CapsRestore()

	" Set relative line number in InsertMode and VisualMode
	autocmd InsertEnter * :setlocal rnu
	autocmd InsertLeave * :setlocal nornu

	function! g:SetLineNumbers()
		if ( mode()=='i' || mode()=='v' )
			setlocal rnu
			setlocal nu
		else
			setlocal nornu
			setlocal nu
		endif
	endfunc

	if !has('autocmd')
		echo 'does not have autocommand!'
	endif

	au WinLeave *    :setlocal nornu nu
	au FocusLost *   :setlocal nornu nu
	au WinEnter *    :call g:SetLineNumbers()
	au FocusGained * :call g:SetLineNumbers()

endif

let VimuxUseNearest = 1
" could use VimuxRunCommandInDir
nmap <leader>cd :call VimuxPromptCommand('cd '.getcwd())<cr><cr>
nmap <leader>d :call VimuxRunCommand("./build.sh -d")<cr>
nmap <leader>r :call VimuxRunCommand("./build.sh -r")<cr>
nmap <leader>D :call VimuxRunCommand("./run.sh -d")<cr>
nmap <leader>R :call VimuxRunCommand("./run.sh -r")<cr>

" clighter
"nmap <leader> cr :call clighter#Rename()<CR>

" fast insert
nmap <leader>, a<C-R>="NOTE(theGiallo): "<cr><Esc>
nmap <leader>. a<C-R>="TODO(theGiallo, ".strftime('%Y-%m-%d')."): "<cr><Esc>

" fold everything
set foldmethod=indent
set foldignore=
set nofoldenable " don't fold by default

" grep
nmap <leader>g :!grep -Hn --color=always

" syntax highlighting for every file type
function! HighlightThings()
	syn keyword	Todo TODO FIXME XXX
	syn keyword	Note NOTE
	syn keyword	Done DONE
	syn keyword	Important IMPORTANT
	syn keyword	Warning WARNING
endfunction
autocmd Syntax * call HighlightThings()
autocmd BufReadPost * call HighlightThings()
autocmd FileType * call HighlightThings()
autocmd BufEnter * call HighlightThings()

" test for long line:                                                                 END
highlight OverLength ctermbg=18 guibg='#383830'
match OverLength /\%81v.\+/



" this is to be able to insert the e-acute character
imap <buffer> è è
