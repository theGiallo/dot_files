# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# to enable del key
tput smkx

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

#Reset
txtrst='\e[0m'
#Regular Colors
txtblk='\e[0;30m'
txtred='\e[0;31m'
txtgrn='\e[0;32m'
txtylw='\e[0;33m'
txtblu='\e[0;34m'
txtpur='\e[0;35m'
txtcyn='\e[0;36m'
txtwht='\e[0;37m'
#Bold Colors
bldblk='\e[1;30m'
bldred='\e[1;31m'
bldgrn='\e[1;32m'
bldylw='\e[1;33m'
bldblu='\e[1;34m'
bldpur='\e[1;35m'
bldcyn='\e[1;36m'
bldwht='\e[1;37m'
#Underlined Colors
undblk='\e[4;30m'
undred='\e[4;31m'
undgrn='\e[4;32m'
undylw='\e[4;33m'
undblu='\e[4;34m'
undpur='\e[4;35m'
undcyn='\e[4;36m'
undwht='\e[4;37m'
#Background Colors
bgblk='\e[40m'
bgred='\e[41m'
bggrn='\e[42m'
bgylw='\e[43m'
bgblu='\e[44m'
bgpur='\e[45m'
bgcyn='\e[46m'
bgwht='\e[47m'

## Base-16 color theme 256 colors
BASE16_SHELL="$HOME/.config/base16-shell/base16-monokai.dark.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

## git prompt
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_SHOWSTASHSTATE=true
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_SHOWCOLORHINTS=true
source /etc/bash_completion.d/git-prompt

git_is_repo()
{
    git rev-parse --is-inside-work-tree &>/dev/null;
}

git_dirty()
{
    # check if we're in a git repo
    git rev-parse --is-inside-work-tree &>/dev/null || return

    # check if it's dirty
    git diff --quiet --ignore-submodules HEAD &>/dev/null;
    if [[ $? -eq 1 ]]; then
        echo -e "$txtred✗$txtrst"
    else
        echo -e "$txtgrn✔$txtrst"
    fi
}

# get the status of the current branch and it's remote
# If there are changes upstream, display a ⇣
# If there are changes that have been committed but not yet pushed, display a ⇡
git_arrows() {
    # do nothing if there is no upstream configured
    git rev-parse --abbrev-ref @'{u}' &>/dev/null || return

    arrows=""
    arrow_status="$(git rev-list --left-right --count HEAD...@'{u}' 2>/dev/null)"

    # do nothing if the command failed
    (( !$? )) || return

    # split on tabs
    IFS='\t' read -r -a arrow_status <<< "$arrow_status"
    #arrow_status=(${(ps:\t:)arrow_status})
    left=${arrow_status[1]}
    right=${arrow_status[2]}

    (( ${right:-0} > 0 )) && arrows+="$txtylw⇣$txtrst"
    (( ${left:-0} > 0 )) && arrows+="$txtblu⇡$txtrst"

    echo -e $arrows
}

git_branch_name()
{
    # check if we're in a git repo
    git rev-parse --is-inside-work-tree &>/dev/null || return

    branch_name=$(git symbolic-ref -q HEAD)
    branch_name=${branch_name##refs/heads/}
    branch_name=${branch_name:-HEAD}

    echo "$branch_name";
}

#if [ "$color_prompt" = yes ]; then
#    PS1="${debian_chroot:+($debian_chroot)}\[$txtwht$bgblk\]\t\[$txtrst\] \[\033[01;32m\]\u@\h\[\033[00m\]: \[\033[01;34m\]\w\[\033[00m\]\$(__git_ps1)\n\$ "
#    PROMPT_COMMAND='RET=$?; if [[ $RET -eq 0 ]]; then echo -ne "\033[0;32m$RET\033[0m (^o^)/"; else echo -ne "\033[0;31m$RET\033[0m (._.)"; fi; echo;'
#else
#    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#fi
if [ "$color_prompt" = yes ]; then
	PS1="${debian_chroot:+($debian_chroot)}\[$txtwht$bgblk\]\t\[$txtrst\] \[\033[01;32m\]\u@\h\[\033[00m\]: \[\033[01;34m\]\w\[\033[00m\] \`git_is_repo && echo -n '['\`\`git_dirty\` \`git_branch_name\`\`git_arrows\`\`git_is_repo && echo -n ']'\`\n\$ "
    PROMPT_COMMAND='RET=$?; if [[ $RET -eq 0 ]]; then echo -ne "\033[0;32m$RET\033[0m (^o^)/"; else echo -ne "\033[0;31m$RET\033[0m (._.)"; fi; echo;'
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
