#!/bin/bash

ALL=false
VIM=false
TMUX=false
BASH=false
ERROR=false
HELP=false

TARGET=$HOME
TARGET_GIVEN=false

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-h|--help)
			HELP=true
		;;
		-v|--vim)
			VIM=true
		;;
		-t|--tmux)
			TMUX=true
		;;
		-b|--bash)
			BASH=true
		;;
		--target)
			shift
			TARGET=$1
			TARGET_GIVEN=true
		;;
		*)
			echo "Unknown option "$1
			ERROR=true
		;;
	esac
	shift
	done;
fi

if $TARGET_GIVEN && ( ! $VIM ) && ( ! $TMUX ) &&  ( ! $BASH )
then
	ALL=true
fi

if $ERROR || $HELP
then
	echo "Usage:"
	echo "install_dot_files.sh [-h|--help] [-v|--vim] [-t|--tmux] [-b|--bash]"
	echo "                     [--target \$TARGET]"
	echo
	echo "    -h|--help            print this help"
	echo
	echo "    -v|--vim             gather vim stuff"
	echo "    -t|--tmux            gather tmux stuff"
	echo "    -b|--bash            gather bash stuff"
	echo
	echo "    --target \$TARGET     sets the install target directory to"
	echo "                         \$TARGET. Usually a home, default is \$HOME"
	echo
	echo "No options gathers all stuff."
	echo "This script backs up old stuff appending a date and '.bkp', e.g. "
	echo "'~/.vim' is backed up as '~/.vim.2017-04-23_15-03-38.bkp'"

	if $ERROR
	then
		exit 1
	fi
	if $HELP
	then
		exit 0
	fi
fi


if $ALL || $VIM
then
	cp "$TARGET/.vimrc" "$TARGET/.vimrc.`date +%Y-%m-%d_%H-%M-%S`.bkp"
	cp -r "$TARGET/.vim" "$TARGET/.vim.`date +%Y-%m-%d_%H-%M-%S`.bkp"
	cp -TRv vim/ "$TARGET"
	vim -c PluginInstall -c PluginUpdate -c qa
fi

if $ALL || $TMUX
then
	cp "$TARGET/.tmux.conf" "$TARGET/.tmux.conf.`date +%Y-%m-%d_%H-%M-%S`.bkp"
	cp -r "$TARGET/.tmux" "$TARGET/.tmux.`date +%Y-%m-%d_%H-%M-%S`.bkp"
	cp -TRv vim/ "$TARGET"
	old_wd="$PWD"
	mkdir -p "$TARGET/.tmux/plugins"
	cd "$TARGET/.tmux/plugins"
	while IFS='' read -r line || [[ -n "$line" ]]
	do
		git clone "$line"
	done < "./tmux/plugins_remotes.txt"
	cd "$old_wd"
fi

if $ALL || $BASH
then
	cp "$TARGET/.bashrc" "$TARGET/.bashrc.`date +%Y-%m-%d_%H-%M-%S`.bkp"
	cp "$TARGET/.profile" "$TARGET/.profile.`date +%Y-%m-%d_%H-%M-%S`.bkp"

	cp bash/.bashrc "$TARGET/"
	cp bash/.profile "$TARGET/"

	git clone https://github.com/chriskempson/base16-shell.git "$TARGET/.config/base16-shell"
fi
